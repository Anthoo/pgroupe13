// pgroupe13.cpp : Defines the entry point for the console application.
#include "stdafx.h"
#include <SFML/Graphics.hpp>
#include "Game.h"
#include "EndGame.h"
#include <vector>
#include "Menu.h"
#include "About.h"

int main()
{

	// setup window On initialise les dimensions de la fenêtre et on lui met un nom. On limite la framerateLimite a 60
	sf::Vector2i screenDimensions(1280, 703);
	sf::RenderWindow window(sf::VideoMode(screenDimensions.x, screenDimensions.y), "Supertes Gloriam");
	window.setFramerateLimit(60);
	Game game;
	game.run(window);
	/*
	//liste des screens
	std::vector<Screen*> screens;
	int screen = 0;

	//Screens preparations
	Menu menu(window.getSize().x, window.getSize().y);
	screens.push_back(&menu);
	About about(window.getSize().x, window.getSize().y);
	screens.push_back(&about);
	EndGame eg(window.getSize().x, window.getSize().y);
	screens.push_back(&eg);
	Game game;
	screens.push_back(&game);
	//Main loop
	while (screen >= 0)
	{
		screen = screens[screen]->run(window);
	}
	*/
}