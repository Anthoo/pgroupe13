#include "stdafx.h"
#include "menu.h"
#include "Game.h"

using namespace std;
using namespace sf;

//Constructeur
Menu::Menu(float width, float height)
{
	//Chargement des ressources graphiques
	//Chargement du background
	if (!backgroundTexture.loadFromFile("../ressources/BackgroundPantheon.png"))
	{
		// Erreur
		cout << "Erreur durant le chargement de l'image de background" << endl;
	}
	else
		background.setTexture(backgroundTexture);


	if (!font.loadFromFile("../ressources/arial.ttf"))
	{
		// Erreur
		cout << "Erreur durant le chargement de arial.ttf" << endl;
	}

	// permet d'appeler les diff�rentes m�thodes lorsque la fenetre endGame est en cours
	this->title = title;
	title.setString("Supertes Gloriam");
	title.setFont(font);
	title.setFillColor(sf::Color::Black);
	title.setCharacterSize(50);
	title.setPosition(sf::Vector2f(width / 2.7, height / 4));

	// manipulation graphique des diff�rents menu de navigation
	menu[0].setString("Play");
	menu[0].setFont(font);
	menu[0].setFillColor(sf::Color::White);
	menu[0].setCharacterSize(40);
	menu[0].setPosition(sf::Vector2f(width / 2, height / (NB_TEXT + 1) * 1.5));

	menu[1].setString("About");
	menu[1].setFont(font);
	menu[1].setFillColor(sf::Color::Black);
	menu[1].setCharacterSize(20);
	menu[1].setPosition(sf::Vector2f(width / 2, height / (NB_TEXT + 1) * 2.5));

	menu[2].setString("Exit");
	menu[2].setFont(font);
	menu[2].setFillColor(sf::Color::Black);
	menu[2].setCharacterSize(20);
	menu[2].setPosition(sf::Vector2f(width / 2, height / (NB_TEXT + 1) * 3.5));

	selectItemIndex = 0;
}

// Destructeur
Menu::~Menu()
{
}

//Fonctions
// Dessiner le titre du jeu et les 3 menus sur la fenetre
void Menu::draw(RenderWindow &window)
{
	window.draw(title);
	for (int i = 0; i < NB_TEXT; i++) {
		window.draw(menu[i]);
	}
}

// permet de faire changer la couleur du texte lorsqu'on appuye sur la fl�che du haut
void Menu::MoveUp() {
	if (selectItemIndex - 1 >= 0) {
		menu[selectItemIndex].setFillColor(sf::Color::Black);
		menu[selectItemIndex].setCharacterSize(20);
		selectItemIndex--;
		menu[selectItemIndex].setFillColor(sf::Color::White);
		menu[selectItemIndex].setCharacterSize(40);

	}
}

// permet de faire changer la couleur du texte lorsqu'on appuye sur la fl�che du bas
void Menu::MoveDown() {
	if (selectItemIndex + 1 < NB_TEXT) {
		menu[selectItemIndex].setFillColor(sf::Color::Black);
		menu[selectItemIndex].setCharacterSize(20);
		selectItemIndex++;
		menu[selectItemIndex].setFillColor(sf::Color::White);
		menu[selectItemIndex].setCharacterSize(40);
	}
}

// Navigation dans le menu principal
// permet d'appeler les diff�rentes m�thodes lorsque la fenetre endGame est en cours
int Menu::run(sf::RenderWindow & app)
{
	bool running = true;
	sf::Event event;

	while (running)
	{
		float y = sf::Joystick::getAxisPosition(0, sf::Joystick::PovY);
		while (app.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::KeyPressed:
				switch (event.key.code)
				{
				case sf::Keyboard::Up:
					MoveUp();
					break;
				case sf::Keyboard::Down:
					MoveDown();
					break;
				case sf::Keyboard::Return:
					if (selectItemIndex == 0)
					{
						//Play
						return -1;
					}
					else if (selectItemIndex == 1)
					{
						// About
						return 1;
					}
					else if (selectItemIndex == 2)
					{
						// Exit
						running = false;
						app.close();
					}
					break;
				case sf::Keyboard::Escape:
					running = false;
					app.close();
					break;
				default:
					break;
				}
				break;
				// Si on appyue sur un bouton d'une manette
			case Event::JoystickMoved:
				if (event.joystickMove.axis == sf::Joystick::PovY)
				{
					if (y > 50)
					{
						MoveUp();
					}
					else if (y < -50)
					{
						MoveDown();
					}
				}
				break;
			case Event::JoystickButtonPressed:
				if (sf::Joystick::isButtonPressed(0, 0))
				{
					if (selectItemIndex == 0)
					{
						return -1;
					}
					else if (selectItemIndex == 1)
					{
						return 1;
					}
					else if (selectItemIndex == 2)
					{
						running = false;
						app.close();
					}
				}
				if (sf::Joystick::isButtonPressed(0, 1))
				{
					running = false;
					app.close();
				}
				break;
			case sf::Event::Closed:
				app.close();
				running = false;
				break;
			}
		}
		app.clear();
		app.draw(background);
		draw(app);
		app.display();


	}
	return (-1);
}