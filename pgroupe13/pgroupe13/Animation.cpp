 ////////////////////////////////////////////////////////////
//Nous avons trouver ce code ici : https://github.com/SFML/SFML/wiki/Source:-AnimatedSprite
//On l'a un peu modifier pour nos besoins
////////////////////////////////////////////////////////////
//
// Copyright (C) 2014 Maximilian Wagenbach (aka. Foaly) (foaly.f@web.de)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
// you must not claim that you wrote the original software.
// If you use this software in a product, an acknowledgment
// in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
// and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "Animation.h"

Animation::Animation() : m_texture(NULL)
{

}
/*Ici, nous ajoutons tout les frames de l'animation. Donc les deux premieres valeurs du IntRect correspond au x et y
  sprite se trouvant sur la spriteSheet. Ensuite nous prenons sa longueur et sa largeur. (Ici nos sprites font du 128x128)
  donc on mettra tout le temps 128 x 128 pour sa longuer et sa largeur. Et pour finir le nombre de frame que constitue l'animation.
*/
void Animation::addFrame(sf::IntRect rect)
{
	m_frames.push_back(rect);
}
/* Permet de changer de texture
*/
void Animation::setSpriteSheet(const sf::Texture& texture)
{
	m_texture = &texture;
}
/*
Permet de r�cup�rer la texture de la spriteSheet
*/
const sf::Texture* Animation::getSpriteSheet() const
{
	return m_texture;
}
/*Permet de r�cup�rer la taille des frames.*/
std::size_t Animation::getSize() const
{
	return m_frames.size();
}
/*Permet de r�cup�rer la frame.*/
const sf::IntRect& Animation::getFrame(std::size_t n) const
{
	return m_frames[n];
}