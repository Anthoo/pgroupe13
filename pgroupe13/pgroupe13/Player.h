#pragma once
#include<string>
using std::string;
class Player
{
private:
	string username,password;
	float score;
	bool admin;
public:
	Player(string username,string password,float score,bool admin);
	~Player();
	string getUsername()const;
	int getScore()const;
	bool isAdmin()const;
	string str()const;
};

