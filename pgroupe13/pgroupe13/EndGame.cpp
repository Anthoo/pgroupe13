#include "stdafx.h"
#include "EndGame.h"
#include <iostream>

using namespace std;
using namespace sf;

EndGame::EndGame(float width, float height)
{
	//Chargement des ressources graphiques
	//Chargement du background
	if (!backgroundTexture.loadFromFile("../ressources/DungeonAbout.png"))
	{
		// Erreur
		cout << "Erreur durant le chargement de l'image de background" << endl;
	}
	else
		background.setTexture(backgroundTexture);


	if (!font.loadFromFile("../ressources/arial.ttf"))
	{
		// Erreur
		cout << "Erreur durant le chargement de arial.ttf" << endl;
	}

	// manipulation graphique du titre
	this->title = title;
	title.setString("End Game");
	title.setFont(font);
	title.setFillColor(sf::Color::White);
	title.setCharacterSize(50);
	title.setPosition(sf::Vector2f(width / 2.2, height / 6));

	// manipulation graphique du bouton retour
	backMenu.setString("Press enter");
	backMenu.setFont(font);
	backMenu.setOrigin(-10, -10);
	backMenu.setFillColor(sf::Color::White);
	backMenu.setPosition(sf::Vector2f(width / 8, height / 1.3));
}


EndGame::~EndGame()
{
}

// permet de dessiner le texte � afficher
void EndGame::draw(RenderWindow &window)
{
	window.draw(title);
	window.draw(backMenu);
}

// permet d'appeler les diff�rentes m�thodes lorsque la fenetre endGame est en cours
int EndGame::run(sf::RenderWindow & app)
{
	sf::Event event;
	bool running = true;

	while (running)
	{
		while (app.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::KeyPressed:
				switch (event.key.code)
				{
				case sf::Keyboard::Return:
					return 0;
					break;
				case sf::Keyboard::Escape:
					running = false;
					app.close();
					break;
				default:
					break;
				}
				break;
				// Si on appyue sur un bouton d'une manette
			case Event::JoystickButtonPressed:
				if (sf::Joystick::isButtonPressed(0, 0))
				{
					return 0;
				}
				break;
			case sf::Event::Closed:
				running = false;
				app.close();
				break;
			}
		}
		app.clear();
		app.draw(background);
		draw(app);
		app.display();

	}

	return (-1);
}