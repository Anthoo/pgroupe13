#include "stdafx.h"
#include "Character.h"
#include "AnimatedSprite.h"
#include <iostream>
/*Constructeur de Character on appelle la texture qui permettra de donner l'apparence au personnage. 
L'id Joystick va �tre utilis� plus tard pour pouvoir jouer avec plusieurs manette.
*/
Character::Character(sf::Texture texture, int idJoystick, float runSpeed, float walkSpeed, bool isJump):movement(0.f, 0.f)
{
	animatedSprite.setFrameTime(sf::seconds(0.05));
	animatedSprite.setLooped(false);
	animatedSprite.stop();
	animatedSprite.setPosition(sf::Vector2f(0,512)); 
	this->texture = texture;
	this->isJumpAvailable = isJump;
	this->runSpeed = runSpeed;
	this->walkSpeed = walkSpeed;
	this->idJoystick = idJoystick;
	this->currentSpeed = walkSpeed;
}
/*Notre personnage meurt si il re�oit un coup donc on return un boolean*/
bool Character::isLife()
{
	return life;
}
/*=============================SetAnimation=================================================================*/
/*Nous allons chercher la texture dans la spriteSheet, ensuite nous prenons la valeur en x et y de l'animation qu'on souhaite avoir,
 ce qui va nous permettre de la retrouver dans la spriteSheet, ensuite il faut d�finir la taille du sprite et comme c'est des sprites de 
 128 par 128 il suffit juste de mettre 128 et pour finir, on met le nombre de frame pour l'animation du perso. En g�n�ral, dans cette application
 il y a 19 frames par animation.
*/
void Character::setRightWalkAnimation(int x, int y, int width, int height, int frameNumber)
{
	rightWalkAnimation.setSpriteSheet(texture);
	for (int i = 0; i < frameNumber; i++) {
		rightWalkAnimation.addFrame(sf::IntRect(x + (i*width), y, width, height));
	}
}

void Character::setRightDanceAnimation(int x, int y, int width, int height, int frameNumber)
{
	rightDanceAnimation.setSpriteSheet(texture);
	for (int i = 0; i < frameNumber; i++) {
		rightDanceAnimation.addFrame(sf::IntRect(x + (i * width), y, width, height));
	}
}

void Character::setRightDieAnimation(int x, int y, int width, int height, int frameNumber)
{
	rightDieAnimation.setSpriteSheet(texture);
	for (int i = 0; i < frameNumber; i++) {
		rightDieAnimation.addFrame(sf::IntRect(x + (i * width), y, width, height));
	}
}

void Character::setRightFallAnimation(int x, int y, int width, int height, int frameNumber)
{
	rightFallAnimation.setSpriteSheet(texture);
	for (int i = 0; i < frameNumber; i++) {
		rightFallAnimation.addFrame(sf::IntRect(x + (i * width), y, width, height));
	}
}

void Character::setRightIdleAnimation(int x, int y, int width, int height, int frameNumber)
{
	rightIdleAnimation.setSpriteSheet(texture);
	for (int i = 0; i < frameNumber; i++) {
		rightIdleAnimation.addFrame(sf::IntRect(x + (i * width), y, width, height));
	}
}

void Character::setRightJumpAnimation(int x, int y, int width, int height, int frameNumber)
{
	rightJumpAnimation.setSpriteSheet(texture);
	for (int i = 0; i < frameNumber; i++) {
		rightJumpAnimation.addFrame(sf::IntRect(x + (i * width), y, width, height));
	}
}

void Character::setRightRunAnimation(int x, int y, int width, int height, int frameNumber)
{
	rightRunAnimation.setSpriteSheet(texture);
	for (int i = 0; i < frameNumber; i++) {
		rightRunAnimation.addFrame(sf::IntRect(x + (i * width), y, width, height));
	}
}

void Character::setRightWalljumpAnimation(int x, int y, int width, int height, int frameNumber)
{
	rightWalljumpAnimation.setSpriteSheet(texture);
	for (int i = 0; i < frameNumber; i++) {
		rightWalljumpAnimation.addFrame(sf::IntRect(x + (i * width), y, width, height));
	}
}

void Character::setRightHitAnimation(int x, int y, int width, int height, int frameNumber)
{
	rightHitAnimation.setSpriteSheet(texture);
	for (int i = 0; i < frameNumber; i++) {
		rightHitAnimation.addFrame(sf::IntRect(x + (i * width), y, width, height));
	}
}

void Character::setLeftWalkAnimation(int x, int y, int width, int height, int frameNumber)//alors comme ca on multiple 0 pd?!
{
	leftWalkAnimation.setSpriteSheet(texture);
	for (int i = 0; i < frameNumber; i++) {
		leftWalkAnimation.addFrame(sf::IntRect(x - (i * width), y, width, height));
	}
}

void Character::setLeftDanceAnimation(int x, int y, int width, int height, int frameNumber)
{
	leftDanceAnimation.setSpriteSheet(texture);
	for (int i = 0; i < frameNumber; i++) {
		leftDanceAnimation.addFrame(sf::IntRect(x - (i * width), y, width, height));
	}
}

void Character::setLeftDieAnimation(int x, int y, int width, int height, int frameNumber)
{
	leftDieAnimation.setSpriteSheet(texture);
	for (int i = 0; i < frameNumber; i++) {
		leftDieAnimation.addFrame(sf::IntRect(x - (i * width), y, width, height));
	}
}

void Character::setLeftFallAnimation(int x, int y, int width, int height, int frameNumber)
{
	leftFallAnimation.setSpriteSheet(texture);
	for (int i = 0; i < frameNumber; i++) {
		leftFallAnimation.addFrame(sf::IntRect(x - (i * width), y, width, height));
	}
}

void Character::setLeftIdleAnimation(int x, int y, int width, int height, int frameNumber)
{
	leftIdleAnimation.setSpriteSheet(texture);
	for (int i = 0; i < frameNumber; i++) {
		leftIdleAnimation.addFrame(sf::IntRect(x - (i * width), y, width, height));
	}
}

void Character::setLeftJumpAnimation(int x, int y, int width, int height, int frameNumber)
{
	leftJumpAnimation.setSpriteSheet(texture);
	for (int i = 0; i < frameNumber; i++) {
		leftJumpAnimation.addFrame(sf::IntRect(x - (i * width), y, width, height));
	}
}

void Character::setLeftRunAnimation(int x, int y, int width, int height, int frameNumber)
{
	leftRunAnimation.setSpriteSheet(texture);
	for (int i = 0; i < frameNumber; i++) {
		leftRunAnimation.addFrame(sf::IntRect(x - (i * width), y, width, height));
	}
}

void Character::setLeftWalljumpAnimation(int x, int y, int width, int height, int frameNumber)
{
	leftWalljumpAnimation.setSpriteSheet(texture);
	for (int i = 0; i < frameNumber; i++) {
		leftWalljumpAnimation.addFrame(sf::IntRect(x - (i * width), y, width, height));
	}
}

void Character::setLeftHitAnimation(int x, int y, int width, int height, int frameNumber)
{
	leftHitAnimation.setSpriteSheet(texture);
	for (int i = 0; i < frameNumber; i++) {
		leftHitAnimation.addFrame(sf::IntRect(x - (i * width), y, width, height));
	}
}
/*=================================================FIN DES SETANIMATION=================================*/
/*Ici on appelle tout les setAnimation pour pouvoir cr�er toutes les animations via le proc�der expliqu� plus haut. 
	C'est plus pratique de le faire en une m�thode que de devoir cr�er 1 m�thodes pour cr�er une seule animation.
*/
void Character::loadAllAnimation() {
	//Left
	setLeftDanceAnimation(2432, 0, 128, 128, 10); 
	setLeftFallAnimation(1152, 0, 128, 128, 10); 
	setLeftDieAnimation(2432, 128, 128, 128, 10);
	setLeftIdleAnimation(1152, 128, 128, 128, 10);
	setLeftHitAnimation(2432, 256, 128, 128, 19);
	setLeftJumpAnimation(2432, 384, 128, 128, 10);
	setLeftWalljumpAnimation(1152, 384, 128, 128, 10);
	setLeftRunAnimation(2432, 512, 128, 128, 19);
	setLeftWalkAnimation(2432, 640, 128, 128, 19);
	//Right
	setRightDanceAnimation(0, 768, 128, 128, 10);
	setRightFallAnimation(1280, 768, 128, 128, 10);
	setRightDieAnimation(0, 896, 128, 128, 10);
	setRightIdleAnimation(1280, 896, 128, 128, 10);
	setRightHitAnimation(0, 1024, 128, 128, 19);
	setRightJumpAnimation(0, 1152, 128, 128, 10);
	setRightWalljumpAnimation(1280, 1152, 128, 128, 10);
	setRightRunAnimation(0, 1280, 128, 128, 19);
	setRightWalkAnimation(0, 1408, 128, 128, 19);
}
/*Permet de r�cup�rer l'animation en cours.*/
Animation* Character::getCurrentAnimation()const {
	return currentAniation;
}
/*C'est dans cette m�thode qu'on permet au personnage de bouger.
	Quant � la gravit�, lorsque le joueur arrive sur les cases (exemple 65) celui-ci se verra 
	augmenter ses y et donc il tombera de plus en plus rapidement jusqu'au moment o� il touchera une
	tile qui n'est pas un tuile dites transparente. Pour savoir quelle tuile est transparente aller voir la TilemapCpp dans la m�thode 
	IsSolide(int coord)
*/
void Character::move(sf::Time &frameTime, Tilemap &map)
{
	noKeyWasPressed = true;
	
	/*Lorsque on utilise le joystick la current animation sera modifier en fonction de l'axe dans laquelle on le pousse donc par exemple
	pour cette m�thode ci, si on pousse le joystick � droite son animation courante va �tre changer par une animation allant vers la droite.
	C'est le m�me proc�d� pour toutes les animations.
	*/
	float x = sf::Joystick::getAxisPosition(this->idJoystick, sf::Joystick::X);// r�cup�re les valeurs de l'axe x
	float z = sf::Joystick::getAxisPosition(this->idJoystick, sf::Joystick::Z);// r�cup�re les valeurs de l'axe z
	if (z < -50) { // si l'axe z <-50, on a appuyer sur la gachette droite, le joueur veut donc courrir
		this->currentSpeed = runSpeed; // on change la vitesse actuelle par la vitesse de course
	}
	else {//sinon on marche
		this->currentSpeed = walkSpeed;//  on change la vitesse actuelle par la vitesse de marche
	}
	if (x < -50) {//si l'axe x <-50 le joueur se d�place vers la gauche
		if (currentSpeed > walkSpeed) {//on verrifie la vitesse actuel pour attribuer la bonne animation
			this->runLeft();//on court
		}
		else {
			this->walkLeft();//on marche
		}
		movement.x = -currentSpeed;//on modifie notre d�placement (n�gatif car on va vers la gauche)
		noKeyWasPressed = false;//on a bouger l'axe
	}
	else if (x > 50) {//si l'axe x > 50 le joueur se d�place vers la droite
		if (currentSpeed > walkSpeed) {//on verrifie la vitesse actuel pour attribuer la bonne animation
			this->runRight();//on court
		}
		else {
			this->walkRight();//on marche
		}
		movement.x = currentSpeed;//on modifie notre d�placement (positif car on va vers la droite)
		noKeyWasPressed = false; //on a bouger l'axe
	}
	else {//sinon l'axe ne sait pas assez d�plac� pour concid�r� qu'il y a eue un mouvement
		movement.x = 0;// on se d�place pas
		noKeyWasPressed = true; //aucune touche n'a �t� d�tect�e
	}
	/*Lorsqu'on presse le boutton y le personnage se met a danser*/
	if (sf::Joystick::isButtonPressed(this->idJoystick, 3))//on verrifie si le bouton 3 a �t� press�
	{
		if (this->isLookingRight()) {//on d�termine l'orientation du personnage pour pouvoir utilis� la bonne animation
			this->danceRight();
		}
		else {
			this->danceLeft();
		}
		noKeyWasPressed = false;
	}
	/*Lorsqu'on presse le boutton X le personnage se met a frapper*/
	if (sf::Joystick::isButtonPressed(this->idJoystick, 2))//on verrifie si le bouton 2 a �t� press�
	{
		noKeyWasPressed = false;
		if (this->isLookingRight()) {//on d�termine l'orientation du personnage pour pouvoir utilis� la bonne animation
			this->hitRight();
		}
		else {
			this->hitLeft();
		}
	}
	if (sf::Joystick::isButtonPressed(this->idJoystick, 0) && isJumpAvailable)//on verrifie si le bouton 0 a �t� press� et si on peut sauter
	{
		isJumpAvailable = false;//on vient de sauter, on ne peut donc plus sauter tant que l'on a pas retoucher le sol -> voir collision
		noKeyWasPressed = false;
		movement.y = -jumpspeed;//on d�place le personnage vers le haut
	}

	/*Lorsqu'aucun bouton n'est press� �a met le sprite en animation "Repos"*/
	if (noKeyWasPressed)
	{
		if (this->isLookingRight()) {//on d�termine l'orientation du personnage pour pouvoir utilis� la bonne animation
			this->idleRight();
		} else if (!this->isLookingRight()) {
			this->idleLeft();
		}
		else if (currentAniation == 0) {//si l'animation en cours est nulle, on vient de cr�er le personnage, on lui mets une animation par d�fault
			this->idleRight();
		}
	}
	/*Permet d'�viter de jump 2 fois*/
	if (!isJumpAvailable) {
		if (this->isLookingRight()) {
			this->jumpRight();
		}
		else {
			this->jumpLeft();
		}
	}
	/*Ensuite, nous faisons appelle � play qui est une m�thode permettant de faire jouer l'animation.*/
	animatedSprite.play(*currentAniation);
	//On v�rifie les collision et on applique la gravit� en fonction de cert
	collision(map);
	
	//move and update
	animatedSprite.move(movement * frameTime.asSeconds());
	/*L'update sera utilis� pour que les frames se mettent correctement lors de l'animation.*/
	animatedSprite.update(frameTime);
}


/*
Alors, pour les collisions d'une tilemap il y avait deux possibilit�s.
La premi�re �tait qu'a chaque fois que le personnage bouge on v�rifie l'enti�ret� des tiles de la map et au moment ou le personnage
rencontre une tile alors il y a collision. Mais il y a un gros probl�me avec cette m�thode. Le calcul qui est fait est beaucoup
trop lourd pour le pc et donc on passe a du 2fps avec juste UN seul personnage qui bouge.
Donc Alexandre et moi avons trouv� une autre solution.
Lorsque le personnage bouge on v�rifie les tuiles qui se trouve autour de lui ce qui demande �norm�ment moins de ressource que l'autre
mani�re.
*/
void Character::collision(Tilemap &map) {
	//on r�cup�re que les tile qui se trouve autour de lui
	int left_tile = (this->getAnimatedSprite().getPosition().x + 64) / 64;
	int right_tile = (this->getAnimatedSprite().getPosition().x + 80) / 64;
	int top_tile = (this->getAnimatedSprite().getPosition().y) / 64;
	int bottom_tile = (this->getAnimatedSprite().getPosition().y + 128) / 64;

	if (left_tile < 0) left_tile = 0;
	if (right_tile > map.getWidth()) right_tile = map.getWidth();
	if (top_tile < 0) top_tile = 0;
	if (bottom_tile > map.getHeight()) bottom_tile = map.getHeight();

	//Correspond au sol de la map.
	int tmpGround = groundHeight;
	for (int i = left_tile; i <= right_tile; i++)
	{
		for (int j = top_tile; j <= bottom_tile; j++)
		{
			//Nous v�rifions si la tuile en contact est solide ou pas.
			if ( map.isSolideTile(i + j * map.getWidth()) )
			{
				tmpGround = j * 64;
				break;
			}
		}
	}
	/*
	contact avec la t�te, le personnage ne peut pas aller plus haut, on applique donc la gravit� pour l'emp�ch� de monter et on retourn pour gagner du temps
	*/
	if (tmpGround <= (top_tile * 64)) {
		movement.y = gravity;
		return;
	}

	int obstacleLeft = -9999;//valeur par d�faut
	int ileft = left_tile - 1;//on regarde la tuile � gauche
	for (int j = top_tile+1; j < bottom_tile; j++)
	{
		if (map.isSolideTile(ileft + j * map.getWidth()))//on v�rrifie si la tuile est solide
		{
			obstacleLeft = ileft * 64;//on r�cup�re l'emplacement de la tuile
			break;
		}
	}
	int obstacleRight = -9999;//valeur par d�faut
	int iright = right_tile + 1;//on regarde la tuile � droite
	{
		for (int j = top_tile+1; j < bottom_tile; j++)
		{
			if (map.isSolideTile(iright + j * map.getWidth()))//on v�rrifie si la tuile est solide
			{
				obstacleRight = iright * 64;//on r�cup�re l'emplacement de la tuile
				break;
			}
		}
	}
	//gravity
	/*Ici, nous v�rifions la position du sprite en lui ajoutant sa hauteur si celle-ci est inf�rieur au background alors,
	  on lui impose la gravit� une gravit�. Nous incr�mentons la gravit� avec un chiffre positif puis que les y de la fen�tre 
	  vont du n�gatifs au positif. Et un incr�mentation du movement.y a �t� faite pour rendre la chute plus r�aliste (plus on tombe
	  haut plus la vitesse d'acc�l�ration de la chute sera grande. 
	*/
	if (animatedSprite.getPosition().y + animatedSprite.getGlobalBounds().height < tmpGround || movement.y<=0) {
		movement.y += gravity;
	}
	//Par contre, si la position du sprite + sa hauteur son plus grand que le sol alors la gravit� ne lui est plus affect�.
	else if (animatedSprite.getPosition().y + animatedSprite.getGlobalBounds().height> tmpGround) {
		animatedSprite.setPosition(animatedSprite.getPosition().x, tmpGround - animatedSprite.getGlobalBounds().height);
		movement.y = 0;// on touche le sol, on ne peux pas s'enfonc�
		isJumpAvailable = true;//on touche le sol, on peut donc saut�
	}
	/*On regarde si on a trouv� un obstacle � gauche*/
	if (obstacleLeft != - 9999) {
		if (animatedSprite.getPosition().x - animatedSprite.getGlobalBounds().width < obstacleLeft) {//si c'est le cas, on regarde si on se trouve sur l'obstacle
			if (movement.x < 0) {//si on va � gauce
				animatedSprite.setPosition(obstacleLeft + 16, animatedSprite.getPosition().y);//on emp�che d'aller � gauche
			}
		}
	}
	/*on regarde si on a trouver un obstacle � droite*/
	if (obstacleRight != - 9999) {
		if (animatedSprite.getPosition().x + animatedSprite.getGlobalBounds().width > obstacleRight) {//si c'est le cas, on regarde si on se trouve sur l'obstacle
			animatedSprite.setPosition(obstacleRight - animatedSprite.getGlobalBounds().width, animatedSprite.getPosition().y);//on emp�che d'aller � droite
		}
	}
}

/*========================Permet de changer toutes les animations courantes.===========================*/
void Character::walkRight() {
	currentAniation = &rightWalkAnimation;
}

void Character::walkLeft() {
	currentAniation = &leftWalkAnimation;
}

void Character::runRight() {
	currentAniation = &rightRunAnimation;
}

void Character::runLeft() {
	currentAniation = &leftRunAnimation;
}

void Character::jumpRight() {
	currentAniation = &rightJumpAnimation;
}

void Character::jumpLeft() {
	currentAniation = &leftJumpAnimation;
}

void Character::walljumpRight()
{
	currentAniation = &rightWalljumpAnimation;
}

void Character::walljumpLeft()
{
	currentAniation = &leftJumpAnimation;
}

void Character::dieRight()
{
	currentAniation = &rightDieAnimation;
}

void Character::dieLeft() {
	currentAniation = &leftDieAnimation;
}

void Character::idleRight() {
	currentAniation = &rightIdleAnimation;
}

void Character::idleLeft() {
	currentAniation = &leftIdleAnimation;
}

void Character::danceRight() {
	currentAniation = &rightDanceAnimation;
}

void Character::danceLeft() {
	currentAniation = &leftDanceAnimation;
}

void Character::fallRight() {
	currentAniation = &rightFallAnimation;
}

void Character::fallLeft() {
	currentAniation = &leftFallAnimation;
}

void Character::hitRight() {
	currentAniation = &rightHitAnimation;
}

void Character::hitLeft() {
	currentAniation = &leftHitAnimation;
}
/*========================Fin des changements d'animations courantes===========================*/

//R�cup�re le sprite anim�
AnimatedSprite Character::getAnimatedSprite() {
	return animatedSprite;
}
/*Permet de savoir si le personnage est orient� vers la droite*/
bool Character::isLookingRight() {
	return currentAniation == &rightWalkAnimation ||
		currentAniation == &rightRunAnimation ||
		currentAniation == &rightIdleAnimation ||
		currentAniation == &rightJumpAnimation ||
		currentAniation == &rightWalljumpAnimation ||
		currentAniation == &rightDieAnimation ||
		currentAniation == &rightDanceAnimation ||
		currentAniation == &rightFallAnimation ||
		currentAniation == &rightHitAnimation;
}
//recup�re l'id du joystick attribuer au character
int Character::getIdJoystick() {
	return this->idJoystick;
}

void Character::resetPosition() {
	animatedSprite.setPosition(sf::Vector2f(0, 512));
}