#include <SFML/Graphics.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include "Screen.h"

#define NB_TEXT 3	// Nombre de menu � afficher

class Menu : public Screen
{

private:
	int selectItemIndex;	// connaitre le menu en cours s�l�ctionn�
	sf::Font font;
	sf::Text menu[NB_TEXT];
	sf::Text title;

	//Background
	sf::Texture backgroundTexture;
	sf::Sprite background;

public:

	Menu(float width, float height);
	~Menu();

	void draw(sf::RenderWindow &window);
	void MoveUp();
	void MoveDown();
	int GetPressedItem() { return selectItemIndex; }
	virtual int run(sf::RenderWindow &App);

};