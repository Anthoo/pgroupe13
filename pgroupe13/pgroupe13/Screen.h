#pragma once
#include <SFML/Graphics.hpp>
class Screen
{
public:
	Screen();
	~Screen();
	virtual int run(sf::RenderWindow &app) = 0;
};

