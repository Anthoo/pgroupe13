#include "stdafx.h"
#include "Tilemap.h"
#include <iostream>
#include "Screen.h"
Tilemap::Tilemap()
{
	
}
//D�truit les tiles. 
Tilemap::~Tilemap()
{
	/*delete tiles;
	tiles = 0;*/
}
/*Permet de charger la map.  
La variable &tileset correspond au chemin pour acc�der � l'image contenant toute nos tuiles.
Notre map fonctionne avec des carr�s 64 sur 64 donc pour la taille de tile on utilise un Vector2U pour qu'il reprenne les dimensions de celle-ci
Pour ce qu iest de la variable tiles celle-ci contient le vecteur contenant tout les chiffres repr�senteront chaque tiles de la tileset.
Par exemple la tile sera trouvant au debut en [0] est la tile se trouvant en haut � gauche de la tileset. C'est ce qui va nous permettre
de choisir ou nous mettrons nos diff�rentes tiles sur la map. Les variables width et height sont la largeur et la hauteur de notre tilemap.

*/
bool Tilemap::load(const std::string & tileset, sf::Vector2u tileSize, int* tiles, unsigned int width, unsigned int height)
{
	this->tiles = tiles;
	this->height = height;
	this->width = width;
	// on charge la texture du tileset
	if (!m_tileset.loadFromFile(tileset))
		return false;

	// on redimensionne le tableau de vertex pour qu'il puisse contenir tout le niveau
	m_vertices.setPrimitiveType(sf::Quads);
	m_vertices.resize(width * height * 4);

	// on remplit le tableau de vertex, avec un quad par tuile
	for (unsigned int i = 0; i < width; ++i)
		for (unsigned int j = 0; j < height; ++j)
		{
			// on r�cup�re le num�ro de tuile courant
			int tileNumber = tiles[i + j * width];

			// on en d�duit sa position dans la texture du tileset
			int tu = tileNumber % (m_tileset.getSize().x / tileSize.x);
			int tv = tileNumber / (m_tileset.getSize().x / tileSize.x);

			// on r�cup�re un pointeur vers le quad � d�finir dans le tableau de vertex
			sf::Vertex* quad = &m_vertices[(i + j * width) * 4];

			// on d�finit ses quatre coins
			quad[0].position = sf::Vector2f(i * tileSize.x, j * tileSize.y);
			quad[1].position = sf::Vector2f((i + 1) * tileSize.x, j * tileSize.y);
			quad[2].position = sf::Vector2f((i + 1) * tileSize.x, (j + 1) * tileSize.y);
			quad[3].position = sf::Vector2f(i * tileSize.x, (j + 1) * tileSize.y);

			// on d�finit ses quatre coordonn�es de texture
			quad[0].texCoords = sf::Vector2f(tu * tileSize.x, tv * tileSize.y);
			quad[1].texCoords = sf::Vector2f((tu + 1) * tileSize.x, tv * tileSize.y);
			quad[2].texCoords = sf::Vector2f((tu + 1) * tileSize.x, (tv + 1) * tileSize.y);
			quad[3].texCoords = sf::Vector2f(tu * tileSize.x, (tv + 1) * tileSize.y);
		}

	return true;
}

void Tilemap::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	// on applique la transformation
	states.transform *= getTransform();

	// on applique la texture du tileset
	states.texture = &m_tileset;

	// et on dessine enfin le tableau de vertex
	target.draw(m_vertices, states);
}


int Tilemap::run(sf::RenderWindow &app)
{
	int level[] =
	 {
		 50, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65,65,65, 65, 65, 65,
		 65,65,65,65,65,65,65,41,40,39,65,65,65,41,40,39,65,65,65,65,
		 0,0,0,0,0,0,1,65,65,28,28,28,28,28,65,65,65,65,65,65 ,
		 4,4,4,4,4,4,5,65,65,21,22,22,22,62,65,65,65,65,65,65,
		 12,4,4,4,4,4,8,65,65,65,65,65,65,65,28,28,28,28,28,28,
		 65,12,4,4,4,8,65,65,65,41,40,39,65,65,21,22,22,22,22,45,
		 64,65,12,9,8,65,65,65,65,65,65,65,65,65,65,65,65,65,65,44,
		 65,65,42,41,40,39,65,65,65,65,65,65,65,65,65,65,28,28,28,44,
		 65,65,65,65,65,65,65,65,65,2,0,0,1,65,65,65,2,0,0,0,
		 65,65,65,65,65,65,65,2,0,4,4,4,5,65,65,65,3,4,4,4,
		 0,0,0,0,0,0,0,4,4,4,4,4,5,65,65,65,3,4,4,63
	 };
	// on cr�e la tilemap avec le niveau pr�c�demment d�fini A su
	std::cout << "bonjour" << std::endl;
	if (!this->load("../ressources/tilesetV2.png ", sf::Vector2u(64, 64), level, 20, 12)){
		return -1;
	}
	app.draw(*this);
	app.display();
	app.clear();
}

int Tilemap::getWidth()
{
	return this->width;
}
int Tilemap::getHeight()
{
	return this->height;
}

int * Tilemap::getTiles()
{
	return this->tiles;
}
/*renvoi vrai si la tile est d�finit comme solide*/
bool Tilemap::isSolideTile(int coord) {
	int value = tiles[coord];
	return !(value == 65 || value == 39 || value == 40 || value == 41 || value == 42 || value == 12 ||
		value == 8 || value == 28 || value==12 || value==9 || value==8 || value == 44 || value == 45 || value ==50);
}
