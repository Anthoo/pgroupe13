#pragma once
#include<string>
#include <SFML/Graphics.hpp>
#include "Screen.h"

class Tilemap : public Screen, public sf::Drawable, public sf::Transformable
{
private:
	sf::VertexArray m_vertices;
	sf::Texture m_tileset;
	int width;
	int height;
	int *tiles;
	sf::Texture backgroundTexture;
public:
	Tilemap();
	~Tilemap();
	//Load the map
	bool load(const std::string& tileset, sf::Vector2u tileSize,int* tiles, unsigned int width, unsigned int height);
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
	virtual int run(sf::RenderWindow &app);
	int getWidth();
	int getHeight();
	int* getTiles();
	bool isSolideTile(int coord);
};

