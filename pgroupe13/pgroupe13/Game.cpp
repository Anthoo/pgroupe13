#include "stdafx.h"
#include "Game.h"
#include <iostream>
#include "stdafx.h"
#include <iostream>
#include "Player.h"
#include "Character.h"
#include <SFML/Graphics.hpp>
#include "AnimatedSprite.h"
#include <iostream>
#include "Tilemap.h"
#include <chrono>
#include "Menu.h"
#include "Tilemap.h"
#include "About.h"
#include "Collision.h"
#include <vector>
#include <cstdlib>
#include <ctime>
#include "EndGame.h"

Game::Game()
{
}


Game::~Game()
{
}

// Cette fonction permet de donner un ordre chronologique au jeu et de charger ses ressources
int Game::run(sf::RenderWindow &app) {
	// load texture (spritesheet) On charge les textures des diff�rents characters. si on se trompe dans le chemin il nous affiche une erreur.
	sf::Texture minotaureTexture;
	if (!minotaureTexture.loadFromFile("../ressources/minotaure.png"))
	{
		std::cout << "Failed to load player spritesheet!" << std::endl;
		return 1;
	}
	sf::Texture singeTexture;
	if (!singeTexture.loadFromFile("../ressources/singe.png"))
	{
		std::cout << "Failed to load player spritesheet!" << std::endl;
		return 1;
	}
	sf::Texture platonTexture;
	if (!platonTexture.loadFromFile("../ressources/platon.png"))
	{
		std::cout << "Failed to load player spritesheet!" << std::endl;
		return 1;
	}
	sf::Texture satyreTexture;
	if (!satyreTexture.loadFromFile("../ressources/satyre.png"))
	{
		std::cout << "Failed to load player spritesheet!" << std::endl;
		return 1;
	}
	sf::Texture cyclopeTexture;
	if (!cyclopeTexture.loadFromFile("../ressources/cyclope.png"))
	{
		std::cout << "Failed to load player spritesheet!" << std::endl;
		return 1;
	}
	//tableau de toute les textures
	sf::Texture textures[] = { minotaureTexture, singeTexture, platonTexture, satyreTexture, cyclopeTexture };

	//liste des joueurs
	std::vector<Character*> players;
	//initialise la liste de joueurs avec les manettes d�j� connect�e
	for (int i = 0; i < 8; i++) {
		if (sf::Joystick::isConnected(i)) { // on v�rrifie si il y a d�j� des manettes branch�es
			std::srand(std::time(nullptr));
			int randomIndex = std::rand() % 5; // g�n�re un nombre al�atoire
			Character* tmp = new Character(textures[randomIndex], i); // attribue un skin al�atoire au personnage et attribue � la manette connect�e
			tmp->loadAllAnimation();
			players.push_back(tmp);// ajout du charact�re dans les players
		}
	}

	// on d�finit le niveau � l'aide de num�ro de tuiles
	int level[] =
	{
		50, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65,65,65, 65, 65, 65,
		65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,
		0,0,0,0,0,1,65,65,65,65,65,65,65,65,65,65,65,65,65,65 ,
		4,4,4,4,4,5,65,65,6,65,65,65,65,65,65,65,65,65,65,65,
		12,4,4,4,4,8,65,65,65,65,65,6,65,65,65,65,65,65,65,65,
		65,12,9,9,8,65,65,65,65,65,65,65,65,65,21,62,65,65,65,65,
		64,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,28,28,
		65,65,65,65,65,65,65,28,28,65,65,65,65,28,28,28,28,28,2,0,
		65,65,65,65,65,28,28,2,1,65,65,65,65,2,0,0,0,0,4,4,
		28,28,28,28,28,2,0,4,5,65,65,65,65,3,4,4,4,4,4,4,
		0,0,0,0,0,4,4,4,5,6,65,65,65,3,4,4,4,4,4,4
	};


	// on cr�e la tilemap avec le niveau pr�c�demment d�fini
	sf::Texture backgroundTexture;
	if (!backgroundTexture.loadFromFile("../ressources/background.png"))
	{
		std::cout << "error" << std::endl;
	}
	sf::Sprite background;
	background.setTexture(backgroundTexture);
	Tilemap map;
	if (!map.load("../ressources/tileset.png ", sf::Vector2u(64, 64), level, 20, 12))
		return -1;

	//liste des screens
	std::vector<Screen*> screens;
	int screen = 0;

	//Screens preparations
	Menu menu(app.getSize().x, app.getSize().y);
	screens.push_back(&menu);
	About about(app.getSize().x, app.getSize().y);
	screens.push_back(&about);
	EndGame eg(app.getSize().x, app.getSize().y);
	screens.push_back(&eg);

	//Main loop
	affiche:
	while (screen >= 0)
	{
		screen = screens[screen]->run(app);
	}

	screen = 0;
	//Permet de mettre a jour les mouvements du joystick
	sf::Joystick::update();
	sf::Clock frameClock;
	//Tant que la fen�tre est ouverte, il y a une succesion d'�venement qui se passe.
	while (app.isOpen())
	{
		sf::Event event;
		while (app.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::KeyPressed:
				switch (event.key.code)
				{
				case sf::Keyboard::Escape:
					app.close();
					break;
				default:
					break;
				}
				break;
			case sf::Event::Closed:
				app.close();
				break;
			case sf::Event::JoystickConnected: {//Donc ici met la texture charg� plus haut sur le character. Ensuite, on lui met un id correspondant au joystick qu'on utilise.
				int id = event.joystickConnect.joystickId; // r�cup�re l'id de la manette connect�e
				std::srand(std::time(nullptr));
				int randomIndex = std::rand() % 5; // g�n�re un nombre al�atoire
				Character* tmp = new Character(textures[randomIndex], id); // attribue un skin al�atoire au personnage et attribue � la manette connect�e
				tmp->loadAllAnimation();
				players.push_back(tmp);// ajout du charact�re dans les players
				break;
			}
			case sf::Event::JoystickDisconnected:
				int idDelete = event.joystickConnect.joystickId; // r�cup�re l'id de la manette d�connect�e
				for (int i = 0; i < players.size(); i++) { // on boucle sur le vecteur de joueurs
					if (players[i]->getIdJoystick() == idDelete) { // on v�rrifie les ids et on supprime le joueurs quand on l'a trouv�
						delete players[i];
						players.erase(players.begin() + i);
						break;
					}
				}
				break;
			}

		}
		//FrameTime du personnage permet de faire apparaitre au bon moment les frames
		sf::Time frameTime = frameClock.restart();

		//On appelle move avec frameTime sur tous les personnages pour que le temps des framtimes soit le m�me pour tout les Characters. La map est utilis� pour les collisions
		for (int i = 0; i < players.size(); i++) {
			players[i]->move(frameTime, map);
		}


		// draw
		//Permet de nettoyer la fen�tre.
		app.clear();
		//Permet de dessiner la map
		app.draw(background);
		app.draw(map);
		//Permet de dessiner les joueurs
		for (int i = 0; i < players.size(); i++) {
			app.draw(players[i]->getAnimatedSprite());
		}
		app.display();
		sf::Vector2f fin(0, 0);//la fin se trouve en haut � droite
		for (int i = 0; i < players.size(); i++) {
			if ((players[i]->getAnimatedSprite().getPosition().x) < (fin.x) && (players[i]->getAnimatedSprite().getPosition().y) < (fin.y)) {
				screen = 2;//on affiche le screen de fin de jeu
				for (int i = 0; i < players.size(); i++) {
					players[i]->resetPosition();//on remets tout les joueurs � leurs position de d�part.
				}
				goto affiche;
			}
		}
	}
	return 3;
}
