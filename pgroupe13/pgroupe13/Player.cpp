#include "stdafx.h"
#include "Player.h"
#include <iostream>
#include <sstream>

//Constructer de player.
Player::Player(string username, string password, float score, bool admin) :username(username), password(password), score(score),admin(admin)
{
}

//Destructeur de player
Player::~Player()
{
}
//Permet de r�cup�rer l'username du player
string Player::getUsername()const
{
	return username;
}
//Permet de r�cup�rer le score du player
int Player::getScore()const
{
	return score;
}
//Permet de savoir s'il est admin ou pas 
bool Player::isAdmin()const
{
	return admin;
}
//Renvoi une chaine de caract�re avec toutes les variables du player
string Player::str() const
{
	std::stringstream ss;
	ss << "Username: " << username << "Password: " << password << "Score: " << score << "Admin: " << admin << std::endl;
	return ss.str();
}
