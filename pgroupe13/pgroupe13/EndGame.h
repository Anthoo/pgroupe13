#pragma once
#include <SFML/Graphics.hpp>
#include"Screen.h"

class EndGame: public Screen
{
private:
	sf::Text title;
	sf::Text backMenu;
	sf::Font font;

	//Background
	sf::Texture backgroundTexture;
	sf::Sprite background;

public:
	EndGame(float width, float height);
	~EndGame();

	void draw(sf::RenderWindow &window);
	virtual int run(sf::RenderWindow &app);
};

