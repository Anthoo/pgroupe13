////////////////////////////////////////////////////////////
//Nous avons trouver ce code ici : https://github.com/SFML/SFML/wiki/Source:-AnimatedSprite
//On l'a un peu modifi� pour nos besoins
////////////////////////////////////////////////////////////
//
// Copyright (C) 2014 Maximilian Wagenbach (aka. Foaly) (foaly.f@web.de)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
// you must not claim that you wrote the original software.
// If you use this software in a product, an acknowledgment
// in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
// and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "AnimatedSprite.h"

/*
Permet de cr�er un sprite anim�.
Nous voulons commencer avec la premi�re frame de l'animation donc frameCurrent est � 0.
*/
AnimatedSprite::AnimatedSprite(sf::Time frameTime, bool paused, bool looped) :
	m_animation(NULL), m_frameTime(frameTime), m_currentFrame(0), m_isPaused(paused), m_isLooped(looped), m_texture(NULL)
{

}
/*
Permet de modifier l'animation du sprite en cours.
Donc on va donner la nouvelle animation en param�tre ensuite,
nous allons changer la texture du sprite avec la nouvelle animation re�ue. 
Nous reprenons la frame a 0 puisque c'est le d�but de l'animation que l'on veut.
Et ensuite on modifie la frame par la frame que nous venons de cr�er.
*/
void AnimatedSprite::setAnimation(const Animation& animation)
{
	m_animation = &animation;
	m_texture = m_animation->getSpriteSheet();
	m_currentFrame = 0;
	setFrame(m_currentFrame);
}
/*
Permet de changer la frameTime de l'animatedSprite
*/
void AnimatedSprite::setFrameTime(sf::Time time)
{
	m_frameTime = time;
}
/*
Permet de faire jouer le personnage.
*/
void AnimatedSprite::play()
{
	m_isPaused = false;
}
/*
Donc ici, on passe une animation en param�tre qui sera l'animation en cours d'utilisation. (voir Character)
On v�rifie si l'animation est diff�rente que celle pass� en param�tre si c'est le cas on la change et on joue l'animation.
*/
void AnimatedSprite::play(const Animation& animation)
{
	if (getAnimation() != &animation)
		setAnimation(animation);
	play();
}
/*Met le sprite en pause*/
void AnimatedSprite::pause()
{
	m_isPaused = true;
}
/*Ici on met le sprite en pause (utilis� quand on relache les touches
Donc on remet la frame � 0 puisqu'on veut qu'il soit au repos.
On change la frame par la frame �tant �gale � 0. Ce qui permettra de rendre 
notre personnage statique lorsque nous ne toucherons plus les touches ou joysticks.
*/
void AnimatedSprite::stop()
{
	m_isPaused = true;
	m_currentFrame = 0;
	setFrame(m_currentFrame);
}
/*
	Permet de changer le fait que �a boucle ou pas.
*/
void AnimatedSprite::setLooped(bool looped)
{
	m_isLooped = looped;
}
/*
	Permet de changer la couleur de l'animatedSprite
*/
void AnimatedSprite::setColor(const sf::Color& color)
{
	// Update the vertices' color
	m_vertices[0].color = color;
	m_vertices[1].color = color;
	m_vertices[2].color = color;
	m_vertices[3].color = color;
}
/*
	 Permet de r�cup�rer l'animation.
*/
const Animation* AnimatedSprite::getAnimation() const
{
	return m_animation;
}
/*
	Permet au personnage d'avoir une hitbox (nous n'avons pas utilis� cette m�thode puisque nous avons cr�� notre 
	propre hitbox puisque la taille de nos personnes sont trop diff�rents de la taille des tuiles de la map.)
*/
sf::FloatRect AnimatedSprite::getLocalBounds() const
{
	sf::IntRect rect = m_animation->getFrame(m_currentFrame);

	float width = static_cast<float>(std::abs(rect.width));
	float height = static_cast<float>(std::abs(rect.height));

	return sf::FloatRect(0.f, 0.f, width, height);
}

/*
Permet au personnage d'avoir une hitbox. La hitbox sera utilis� pour effectuer la gravit� sur le personnage
*/
sf::FloatRect AnimatedSprite::getGlobalBounds() const
{
	return getTransform().transformRect(getLocalBounds());
}

/*
	Permet de r�cup�rer l'animation en cours.
*/
std::size_t AnimatedSprite::getCurrentFrame() const
{
	return m_currentFrame;
}

/*
	Permet de savoir si �a boucle ou pas.
*/
bool AnimatedSprite::isLooped() const
{
	return m_isLooped;
}
/*
	Permet de savoir si ca joue ou pas.
*/
bool AnimatedSprite::isPlaying() const
{
	return !m_isPaused;
}
/*
	R�cup�rer la frameTime
*/
sf::Time AnimatedSprite::getFrameTime() const
{
	return m_frameTime;
}
/*
	Permet de modifier les frames.
*/
void AnimatedSprite::setFrame(std::size_t newFrame, bool resetTime)
{
	if (m_animation)
	{
		//Calcul les nouvelles positions du vertex et coordonn�es des textures.
		sf::IntRect rect = m_animation->getFrame(newFrame);

		/*JP*/
		m_vertices[0].position = sf::Vector2f(0.f, 0.f);
		m_vertices[1].position = sf::Vector2f(0.f, static_cast<float>(rect.height));
		m_vertices[2].position = sf::Vector2f(static_cast<float>(rect.width), static_cast<float>(rect.height));
		m_vertices[3].position = sf::Vector2f(static_cast<float>(rect.width), 0.f);

		float left = static_cast<float>(rect.left) + 0.0001f;
		float right = left + static_cast<float>(rect.width);
		float top = static_cast<float>(rect.top);
		float bottom = top + static_cast<float>(rect.height);

		/*Donne les nouvelles coordonn�es du vexter.*/
		m_vertices[0].texCoords = sf::Vector2f(left, top);
		m_vertices[1].texCoords = sf::Vector2f(left, bottom);
		m_vertices[2].texCoords = sf::Vector2f(right, bottom);
		m_vertices[3].texCoords = sf::Vector2f(right, top);
	}
	
	if (resetTime)
		m_currentTime = sf::Time::Zero;
}
/*Renvoi le sprite de l'animation*/
sf::Sprite AnimatedSprite::getSprite()
{
	sf::Sprite sprite(*m_animation->getSpriteSheet(), m_animation->getFrame(this->getCurrentFrame()));
	sprite.setPosition(this->getPosition());
	return sprite;
}

void AnimatedSprite::update(sf::Time deltaTime)
{
	// Si ce n'est pas en pause nous avons une animation valide.
	if (!m_isPaused && m_animation)
	{
		//Nous ajoutons du temps au temps courrant
		m_currentTime += deltaTime;

		// Si le temps courrant est plus grand alors la frame time avance d'une frame.
		if (m_currentTime >= m_frameTime)
		{
			/*R�initialise le temps mais garde le reste. */
			m_currentTime = sf::microseconds(m_currentTime.asMicroseconds() % m_frameTime.asMicroseconds());

			/* Permet de r�cup�rer l'index de la prochaine frame*/
			if (m_currentFrame + 1 < m_animation->getSize())
				m_currentFrame++;
			else
			{
				// L'animation est fini
				m_currentFrame = 0; // Remise � 0.

				/*On peut mettre l'animation en pause*/
				if (!m_isLooped)
				{
					m_isPaused = true;
				}

			}

			
			// On change l'animation courante.
			setFrame(m_currentFrame, false);
		}
	}
}
/*
	Permet de dessiner le sprite anim�.
*/
void AnimatedSprite::draw(sf::RenderTarget& target, sf::RenderStates states) const	
{
	if (m_animation && m_texture)
	{
		states.transform *= getTransform();
		states.texture = m_texture;
		target.draw(m_vertices, 4, sf::Quads, states);
	}
}
