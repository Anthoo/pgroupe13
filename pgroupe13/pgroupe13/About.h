#pragma once
#include <SFML/Graphics.hpp>
#include "Screen.h"

#define NB_TEXT 5	// // Nombre d'�l�ments � afficher

class About : public Screen
{
private:
	sf::Text title;
	sf::Text backMenu;
	sf::Font font;
	sf::Text texte[NB_TEXT];

	//Background
	sf::Texture backgroundTexture;
	sf::Sprite background;

public:
	About(float width, float height);
	~About();

	void draw(sf::RenderWindow &window);
	virtual int run(sf::RenderWindow &app);
};

