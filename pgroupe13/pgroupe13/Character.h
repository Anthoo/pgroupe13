#pragma once
#include <SFML/Graphics.hpp>
#include "AnimatedSprite.h"
#include "Tilemap.h"
class Character
{
private:

	bool life, isJumpAvailable, noKeyWasPressed;
	sf::Texture texture;
	Animation leftWalkAnimation, leftRunAnimation, leftJumpAnimation, leftWalljumpAnimation, leftDieAnimation, leftIdleAnimation, leftDanceAnimation, leftFallAnimation, leftHitAnimation,
		rightWalkAnimation, rightRunAnimation, rightJumpAnimation, rightWalljumpAnimation, rightDieAnimation, rightIdleAnimation, rightDanceAnimation, rightFallAnimation, rightHitAnimation;
	Animation* currentAniation;
	AnimatedSprite animatedSprite;
	float walkSpeed, runSpeed, currentSpeed, jumpspeed=380.0f, gravity= 10.0f, groundHeight=800;
	sf::Clock frameClock;
	sf::Time frameTime;
	int idJoystick;
	sf::Vector2f movement;

public:

	Character(sf::Texture texture, int idJoystick, float runSpeed = 300.f, float walkSpeed =150.f, bool isJump = true);

	bool isLife();
	/*Set all animations sprite*/
	//left
	void setLeftWalkAnimation(int x, int y, int width, int height, int frameNumber);
	void setLeftRunAnimation(int x, int y, int width, int height, int frameNumber);
	void setLeftJumpAnimation(int x, int y, int width, int height, int frameNumber);
	void setLeftWalljumpAnimation(int x, int y, int width, int height, int frameNumber);
	void setLeftDieAnimation(int x, int y, int width, int height, int frameNumber);
	void setLeftIdleAnimation(int x, int y, int width, int height, int frameNumber);
	void setLeftDanceAnimation(int x, int y, int width, int height, int frameNumber);
	void setLeftFallAnimation(int x, int y, int width, int height, int frameNumber);
	void setLeftHitAnimation(int x, int y, int width, int height, int frameNumber);
	//right
	void setRightWalkAnimation(int x, int y, int width, int height, int frameNumber);
	void setRightRunAnimation(int x, int y, int width, int height, int frameNumber);
	void setRightJumpAnimation(int x, int y, int width, int height, int frameNumber);
	void setRightWalljumpAnimation(int x, int y, int width, int height, int frameNumber);
	void setRightDieAnimation(int x, int y, int width, int height, int frameNumber);
	void setRightIdleAnimation(int x, int y, int width, int height, int frameNumber);
	void setRightDanceAnimation(int x, int y, int width, int height, int frameNumber);
	void setRightFallAnimation(int x, int y, int width, int height, int frameNumber);
	void setRightHitAnimation(int x, int y, int width, int height, int frameNumber);

	void loadAllAnimation();

	void move(sf::Time &frameTime, Tilemap &map);
	void collision(Tilemap &map);
	
	Animation* getCurrentAnimation()const;
	AnimatedSprite getAnimatedSprite();
	void walkRight();
	void walkLeft();
	void runRight();
	void runLeft();
	void jumpRight();
	void jumpLeft();
	void walljumpRight();
	void walljumpLeft();
	void dieRight();
	void dieLeft();
	void idleRight();
	void idleLeft(); 
	void danceRight();
	void danceLeft();
	void fallRight();
	void fallLeft();
	void hitRight();
	void hitLeft();

	bool isLookingRight();

	int getIdJoystick();

	void resetPosition();
};

