#pragma once
#include <SFML/Graphics.hpp>
#include "Screen.h"
#include "Tilemap.h"
#include "Character.h"
class Game : public Screen
{
public:
	Game();
	~Game();
	virtual int run(sf::RenderWindow &App);
};

