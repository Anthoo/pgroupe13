#include "stdafx.h"
#include "About.h"
#include <iostream>

using namespace std;
using namespace sf;


About::About(float width, float height)
{
	//Chargement des ressources graphiques
	//Chargement du background
	if (!backgroundTexture.loadFromFile("../ressources/DungeonAbout.png"))
	{
		// Erreur
		cout << "Erreur durant le chargement de l'image de background" << endl;
	}
	else
		background.setTexture(backgroundTexture);


	if (!font.loadFromFile("../ressources/arial.ttf"))
	{
		// Erreur
		cout << "Erreur durant le chargement de arial.ttf" << endl;
	}

	// manipulation graphique du titre
	this->title = title;
	title.setString("About");
	title.setFont(font);
	title.setFillColor(sf::Color::White);
	title.setCharacterSize(50);
	title.setPosition(sf::Vector2f(width / 2.2, height / 6));

	// manipulation graphique des informations
	texte[0].setString("Jeu r�alis� par");
	texte[0].setFont(font);
	texte[0].setFillColor(sf::Color::White);
	texte[0].setCharacterSize(20);
	texte[0].setPosition(sf::Vector2f(width / 2.2, height / (NB_TEXT + 1) * 2));

	texte[1].setString("Cabello Anthony,");
	texte[1].setFont(font);
	texte[1].setFillColor(sf::Color::White);
	texte[1].setCharacterSize(20);
	texte[1].setPosition(sf::Vector2f(width / 2.2, height / (NB_TEXT + 1) * 2.5));

	texte[2].setString("Ledrich FLorian,");
	texte[2].setFont(font);
	texte[2].setFillColor(sf::Color::White);
	texte[2].setCharacterSize(20);
	texte[2].setPosition(sf::Vector2f(width / 2.2, height / (NB_TEXT + 1) * 3));

	texte[3].setString("Pagnieau Alexandre,");
	texte[3].setFont(font);
	texte[3].setFillColor(sf::Color::White);
	texte[3].setCharacterSize(20);
	texte[3].setPosition(sf::Vector2f(width / 2.22, height / (NB_TEXT + 1) * 3.5));

	texte[4].setString("�tudiants de la HELHa Campus Mons, dans le cadre du cours de Mr. V. Altares.");
	texte[4].setFont(font);
	texte[4].setFillColor(sf::Color::White);
	texte[4].setCharacterSize(20);
	texte[4].setPosition(sf::Vector2f(width / 4, height / (NB_TEXT + 1) * 4));

	// manipulation graphique du bouton retour
	backMenu.setString("Press enter");
	backMenu.setFont(font);
	backMenu.setOrigin(-10, -10);
	backMenu.setFillColor(sf::Color::White);
	backMenu.setPosition(sf::Vector2f(width / 8, height / 1.3));

}


About::~About()
{
}

// permet de dessiner le texte � afficher 
void About::draw(RenderWindow &window)
{
	window.draw(title);
	for (int i = 0; i < NB_TEXT; i++) {
		window.draw(texte[i]);
	}
	window.draw(backMenu);
}

// permet d'appeler les diff�rentes m�thodes lorsque la fenetre endGame est en cours
int About::run(sf::RenderWindow & app)
{
	sf::Event event;
	bool running = true;

	while (running)
	{
		while (app.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::KeyPressed:
				switch (event.key.code)
				{
				case sf::Keyboard::Return:
					return 0;
					break;
				case sf::Keyboard::Escape:
					running = false;
					app.close();
					break;
				default:
					break;
				}
				break;
				// Si on appyue sur un bouton d'une manette
			case Event::JoystickButtonPressed:
				if (sf::Joystick::isButtonPressed(0, 0))
				{
					return 0;
				}
				break;
			case sf::Event::Closed:
				running = false;
				app.close();
				break;
			}
		}
		app.clear();
		app.draw(background);
		draw(app);
		app.display();

	}

	return (-1);
}

